# Command Generic Story

Generic Lambda Story is the source code to generate the Coding Story named: 
"Generic Lambda Story Java"

This story tells about improving an application using generics with a form to
do generic dynamic command into a version that uses lambda expressions.

This will be translated by program [md2cs](https://github.com/jfcmacro/md2cs).

We assumed that you had installed the program [md2cs](https://github.com/jfcmacro/md2cs)

```shell
$ cd <dir where this repo is installed>
$ md2cs story.md
$ mv target/command-generic-story-java
$ git push
```

