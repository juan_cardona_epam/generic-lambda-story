---
repository: https://github.com/jfcmacro/commandlambdastream.git
tag: generics-v2.1
origin: git@gitlab.com:jfcmacro/generic-lambda-story-java.git
---
# Generic Lambda Story

**Estimated reading time**: 25 minutes

## Story Outline

The Command Pattern can be used as parameterized computation, enabling us to dynamically change computation without recompiling the application. When it is mixed with generics, it allows the definition of generic behavior as dynamic functions. This feature is fantastic because it avoids a lot of boilerplate code. The programmer doesn't create a specific class for each type needed to pass as a parameter or return a value. But, the generics used in this way introduce a new kind of boilerplate code.

In this story,  we will show how to avoid introducing new boilerplate code using generics with the Command pattern by implementing an application that implements the essential services of a database based on spreadsheets (`forEach`, `filter`, `map`, and `reduce`). We will show how to use lambda expressions to avoid that boilerplate code.

## Story Organization
**Story Branch**: main

> `git checkout main`

**Practical task tag for self-study**: task

> `git checkout task`

Tags: #command_design_pattern, #generics, #lambda_functions, #streams

---
focus:
---

### Generic Lambda Stream Story

When the command pattern was defined for Object-Oriented Programming Languages, it enabled static programming languages could define dynamic behaviors, which was great for creating dynamic applications. At the time of its definition, many object-oriented languages suffered from a lack of code generalization mechanisms, which caused implementing the Command pattern many problems due to boilerplate code. So, when generics appeared in languages like Java, this made it possible to simplify the construction of the Command pattern and, through a series of generic interfaces, to define much of the expected behavior.

To implement a dynamic application, a programmer only has to implement a series of interfaces that facilitate describing a dynamic computation, instantiating the corresponding behavior, and calling each method to obtain the dynamic computation. This has to be done for each dynamic behavior. We have already introduced more boilerplate code.

This story will tell how to eliminate that boilerplate code using something from the Functional Programming Paradigm: lambda expressions.

Our story will start by showing how to implement an essential service of a
table-based database similar to the one implemented in the
spreadsheets through a service that will allow you to iterate from
different ways:

* `forEach` applies a computation with side effects to each database item.
* `filter` through a predicate determines which elements are left in the database.
* `map` iterates over each item in the database producing a new base with a different type.
* `reduce` iterates over the database values producing a result.

We will implement these functions like this:

* Through the Command design pattern with generics.
* Through lambdas expressions that our command replaces.

---
focus: src/main/java/com/epam/rd/cls/SalesSummaryRow.java
---

### Database Description

Our database design is based on a set of sales summarized on a table, where each row ([`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java)) contains the sales of one article ([`Articles`](src/main/java/com/epam/rd/cls/Article.java)) on a region ([`Region`](src/main/java/com/epam/rd/cls/Region.java)) during a period. The database is generated through a helper class [`DBHelper`](src/main/java/com/epam/rd/cls/DBHelper.java) which returns a list of type values [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java), formally: `List<SalesSummaryRow>` is the type of our database data.

**Note:** Although we are already using generics with the type List, we do it because we do not want to add nongeneric code for lists, bloating our unnecessary code, which is already more significant than we expect.

---
focus: src/main/java/com/epam/rd/cls/Command.java
---

### It Needs to be More Generic, My Command!

We will start with the `Command` interface so that it can return values; for this, we have added the variable you type `R` to indicate that it will produce a value of any type.

---
focus: src/main/java/com/epam/rd/cls/SetCmd.java:5-7
---

### Passing Generics Arguments

Now, we can return a value from computation and set an argument. We achieve this by introducing the variable of type `T`, which indicates that it can receive any type. This can be dynamically input to the computation through the invocation of the `set` method.

A function that receives a `String` and converts it to an integer could be implemented like this:

```java
SetCmd<String, Integer> sc = new SetCmd<String, Integer> {
   private String value;
   public void set(String value) {
     this.value = value;
   }
   public Integer execute() {
      return Integer.parseInt(value);
   }
}
```

You can create instances of any type in a more straightforward way. Note the diamond operator (`<>`):

```java
SetCmd<String, Integer> sc = new SetCmd<>() {
   private String value;
   public void setValue(String value) {
      this.value = value;
   }
   public Integer execute() {
      return Integer.parseInt(value);
   }
}
```

Now, do we have a more flexible version? It makes a little more sense starting from a generic interface. We have infinite forms of representation, but what happens if the previous code returns a Double instead of returning an Integer? We have to rewrite some parts of the former code.


---
focus: src/main/java/com/epam/rd/cls/BiSetCmd.java
---

### More parameters

What if I want more parameters?

There are two alternatives. The first is to pass an array of objects (`Object`) and perform the conversion manually for each corresponding type, with the code repeated in each case.

The second manner is an intermediate way: creating a new generic interface for the number of arguments; those arguments are generic too. For instance, if we need two input arguments, we make an interface called `BiSetCmd<S, T, R>`, where `S` and `T` are input arguments, and `R` is the result. If do we need three input arguments? `TriSetCmd<P, S, T, R>`, where `P`, `S`, `T` are the input arguments, and `R` is the result. We use the same strategy for 4, 5, and so. What is the advantage of doing it in this manner? The compiler will check the implementation of that interface, and the type system will check its corresponding parameters; we can be sure that the instance has parameters belonging to the same type.

We will use the second manner.

---
focus: src/main/java/com/epam/rd/cls/ForEachCmd.java
---

### `forEach` Generic Service

Having our generic base classes: [`Command`](src/main/java/com/epam/rd/cls/Command.java), [`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java), and [`BiSetCmd`](src/main/java/com/epam/rd/cls/BiSetCmd.java), we can implement each of the base services of data generically.

We start with the class [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java). This class is parameterized in two elements: `T`, which represents the elements of the database, and `R`, which can be the value of return, although, for the proposed model, this last parameter is not required and may be instantiated to a value of type `Void`. `Void` is a class that represents an empty value.

The constructor of [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) receives the database and the function (an instance of [`SetCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java)) representing a side effect computation. It can be seen as [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) executed by iterating through each element, applying to each piece of the database the function (or procedure if it returns an instance of `Void`) that function or method implements the interface [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java).

---
focus: src/main/java/com/epam/rd/cls/FilterCmd.java
---

### `filter` Generic Service

Now, let's look at the service implementation of the filter ([`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCmd)).
The constructor [`FilterCmd`](src/main/java/com/epam/rd/cls/FilterCmd:10-14) is very similar to the above implementation of [`ForEachCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java), but in this case, the second generic argument of [`SetCmd`](src/main/java/com/epam/rd/cls/ForEachCmd.java) is concretized with a `Boolean` type. That represents a predicate that verifies if a value of type `T` accomplishes one specific criterion.
The following part shows the implementation of the method [`execute`](src/main/java/com/epam/rd/cls/FilterCmd:10-14), this function returns a new database with values from the same type, but that fulfills the predicate ([`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java)).

---
focus: src/main/java/com/epam/rd/cls/MapCmd.java
---

###  `map` Generic Service

The map ([`MapCmd`](src/main/java/com/epam/rd/cls/MapCmd.java)) generic service implementation follows a very similar line to the two previous generic solutions. The determining elements are the function represented by an instance of [`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java) it is not the type is fixed, and the result is of interest since this will be part of the result that is a new database; we can observe this in the implementation of the method [`execute`](src/main/java/com/epam/rd/cls/MapCmd.java:17-24), which takes care of accumulating and adding each result to the return list (`retList`).

---
focus: src/main/java/com/epam/rd/cls/ReduceCmd.java
---

### `reduce` Generic Service

The reduce ([`ReduceCmd`](src/main/java/com/epam/rd/cls/ReduceCmd.java:12-18)) service has significant changes in the constructor; not only does it receive the database, but it also receives an instance of the [`BiSetCmd`](src/main/java/com/epam/rd/cls/BiSetCmd.java) interface that is the representation of an operator while receiving the default value of the operation. The implementation of the function [`execute`](src/main/java/com/epam/rd/cls/ReduceCmd.java:20-27) It can be seen how, through the [`BiSetCmd`](src/main/java/com/epam/rd/cls/BiSetCmd.java) type operator, it allows "accumulate" the result in each traversal on the database and at the end return said "accumulator".

---
focus: src/main/java/com/epam/rd/cls/Main.java
---

### Database Implementation with Generic Service

Let's look at how the database is implemented using the generic services.

* [`forEach`](src/main/java/com/epam/rd/cls/Main.java:7-12:old)
* [`filter`](src/main/java/com/epam/rd/cls/Main.java:28-35:old)
* [`map`](src/main/java/com/epam/rd/cls/Main.java:37-55:old)
* [`reduce`](src/main/java/com/epam/rd/cls/Main.java:57-67:old)

As we've already seen, the generic code service is more flexible when it is concretized with a specific type.

---
focus: src/main/java/com/epam/rd/cls/Main.java:51-60
---

### Implementation of Predicates

The construction of a predicate or any other computation is done by implementing the [`SetCmd`](src/main/java/com/epam/rd/cls/SetCmd.java) interface defining the types that go to work, either via defining an anonymous class or a class that implements that interface. For instance, the implementation of [`PredSouthCmd`](src/main/java/com/epam/rd/cls/PredSouthCmd.java) shows how it can be done. In the same way, the other necessary computations can be implemented following the same strategy on the defined services: `forEach`, `filter`, `map`, and `reduce`.

---
focus:
---

### Summary of a Generic Command Way.

We have simplified the construction of computations by using generics. But even though classes have been reduced to implement the different ones, there are still problems:

* Precision, some implementations will require to be more verbose, although less than the non-generic ones, they are still very imprecise.
* Flexibility, since you have to use repeated code to define each computation.

The following section shows how to reduce these problems using lambda expressions.

---
tag: lambda-v2.0
focus:
---

### A Lambda Expressions Way

Using the Command design pattern allowed us to store a computation, but this is an artificial construction. Nonetheless, we can define new computations and pass and obtain them dynamically; they are classes with methods. To use them, we need to define the class and instantiate it.
We require a new constructor within the language that provides us to describe these computations more precisely and flexibly.
The exact purpose of storing, passing, and using a computation can be achieved through a concept defined within functional programming: lambda expressions.
A lambda expression is a segment of code that can be stored, passed, and ran it.
The following is the syntax to define a lambda expression in Java (also named anonymous function).

```java
(Arguments) -> Function Body
```

If we compare it with a method within a class:

```java
public R functionName(Arguments) {
    // Function Body
}
```

* There are no explicit access modifiers (`public`, `protected`, `private`) of the function.
* There is no explicit return type; this is inferred from the function body.
* `->` is an operator that separates the body arguments from the function
* There is no need to define a class, and there is no need to instantiate.

> A lambda expression represents a computation.

---
focus: src/main/java/com/epam/rd/cls/Main.java:12-19
---

### `forEach` Lambda Service

Let's apply lambda expressions to the first element of the service in our database: [`foreach`](src/main/java/com/epam/rd/cls/Main.java:12-19). If we remember, we use a procedure defined by the `SetCmd<T,R>` interface in which we set the type parameter `R` to the type `Void` to make it a process. With lambda expressions, a procedure is represented by the `Consumer<T>` interface, defined as a functional interface `@FunctionalInterface`, which accepts a value and returns no result.

To concretize this instance, we don’t have to create an anonymous class or create a named class that implements this interface, although you can if you wish; it is better to specify it through a lambda expression that does the same as seen [here](src/main/java/com/epam/rd/cls/Main.java:57-58).

The lambda expression follows the following format:

```java
s -> System.out.println(s)
```

* If the parameter list contains a single parameter, you can omit parentheses.
* If the lambda expression's body only contains a single instruction, braces may be omitted.
* The lambda expression has only one parameter.
* The `println` method returns `void`.
* Therefore complies with the `Consumer` interface.


---
focus: src/main/java/com/epam/rd/cls/Main.java:20-31
---

### `filter` Lambda Database

Our previous implementation (filter generic service) required concretizing an instance of the `SetCmd<T, R>` interface with the variable `R` replaced with a value of type Boolean to represent a predicate.

With lambda expression, there is an interface called `Predicate<T>` that takes a value of type `T`, evaluates it, and determines whether or not it satisfies the predicate.

As we did before, we will concretize an instance of `Predicate<T>` using a lambda expression that takes a value and produces a result of type [`Boolean`](src/main/java/com/epam/rd/cls/Main.java:60). The argument `s`, with no type set, is inferred to be of type [`SalesSummaryRow`](src/main/java/com/epam/rd/cls/SalesSummaryRow.java), its method is applied to it, and it is compared, whose result is as expected.

---
focus: src/main/java/com/epam/rd/cls/Main.java:32-40
---

### `map` Lambda Database

The map implementation requires a computation that takes a value of one type and produces another (not necessarily a different) type.

In our previous version, we concreted an instance of the interface `SetCmd<T,R>`. With lambda, an example of the interface is used `Function<T,R>`, which accepts an argument of type `T` and returns a value of type `R`.

In this case, we use a lambda expression as follows:

```java
s -> s.getInitialUnits()
```

Where the variable is of type `SalesSummaryRow` and transforms it into a value integer type.

---
focus: src/main/java/com/epam/rd/cls/Main.java:42-50
---

### `reduce` Lambda Database

The implementation of the reduce function requires the use of an operator. In our previous case, we used the interface [`BiSetCmd<T,S,R>`](src/main/java/com/epam/rd/cls/BiSetCmd.java:old), which we concretize in a class of the form `BiSetCmd<T,R,R>` where `T` is the type from which we extract a value of type `R`. We produce a result of the same type `R` for that becomes an operator.

Lambda expressions have a similar function that takes care of performing said task `BiFunction<T,S,R>`, which we will handle in this case we will concretize it to a lambda expression that is an instance of `BiFunction<SalesSummaryRow,Double,Double>`. In this case, the lambda expression will be as follows:

```java
(s,d) -> s.profit() + d
```

---
focus:
---

### Summary of Lambda Way

As we did in our previous version, lambda expressions bring a series of interfaces that support the different models of functions; these are defined in the package: `java.util.function,` among which we have used.

The following is the group of interfaces used.

* `Consumer<T>` represents a computation that receives a value and does not produces a result.
* `Predicate<T>`represents a computation that receives a value and verifies a property (predicate).
* `Function<T,R>` represents a computation that receives a value and returns a value.
* `BiFunction<T,S,R>` represents a computation that receives two parameters and returns a value.

Other interfaces in the package can be used in other cases.

---
focus:
---

### Conclusions

* We have shown a way of like a fully oriented world to objects we can
  use the characteristics that exist in functional languages, such
  as lambdas expressions.

* Generality not only works in the early stages of object-oriented
  programming but is also the fundamental basis of programming with
  lambdas expressions.
